Docker Zitadel
=========

Install and configure Zitadel on a docker host.

Requirements
------------

A ready setup docker host is required. You could use my docker role to accomplish that.

Collections:
- community.docker

Roles:
 - vestern.docker



Role Variables
--------------


Dependencies
------------

None

Example Playbook
----------------

    - hosts: servers
      roles:
        - vestern.docker
        - vestern.docker-cockroachdb
        - vestern.docker-zitadel


License
-------

BSD

Author Information
------------------

Website: https://vestern.se

